const unzip = function (arr) {
  const arr1 = [];
  const arr2 = [];
  const arr3 = [];
  arr.flat().forEach((element) => {
    if (typeof element === "string") {
      arr1.push(element);
    } else if (typeof element === "number") {
      arr2.push(element);
    } else {
      arr3.push(element);
    }
  });
  return [arr1, arr2, arr3];
};

console.log(
  unzip([
    ["a", 1, true],
    ["b", 2, false],
  ])
);
console.log(
  unzip([
    ["a", 1, true],
    ["b", 2],
  ])
);
